# SNS Backend API - get_brands

## Implementation Notes

### Get sns social brand list

---------------------------------------------
```
Request Parameters -
wisers_kol_id: KOL ID of Wisers


Sample Request:
{
    "wisers_kol_id":1
}

-------------------------------------
Response Parameters
    brandList: List

Response Class

Model Schema
{
    "brandList": [
        {
            "category": "",
            "name": "",
            "brands": [
                {
                    "id": "",
                    "name": ""
                }
            ]
        }
    ],
    "returnCode": "",
    "action": ""
}

returnCode -
50000: Success
40000: Fail
E10001: Timeout
E10004: request param null
```


### Sample Code in Swift

```
public func get_brands(wisers_kol_id:Int, completionHandler:@escaping (_ result:[[String:AnyObject]]?) -> Void) {
    if noNetwork {
        return
    }

    let body:[String:Any] = [
        "wisers_kol_id": wisers_kol_id,
    ]

    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))

        var request = URLRequest(url: URL(string: "get_brands", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData

        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let result = jsonDic["brandList"] as? [[String:AnyObject]] {
                            completionHandler(result)
                        }
                        else {
                            completionHandler([])
                        }                        
                        return
                    }
                    else {
                        checkReturnCode("get_brands", code: code)
                    }
                }
            }
            else {
                print("get_brands", error)
            }

            completionHandler(nil)
        }).resume()
    }
    catch {
    }
}

```
