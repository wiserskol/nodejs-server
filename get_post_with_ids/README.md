# SNS Backend API - get_post_with_ids

## Implementation Notes
Get sns social post list by ids

This API is a modified version from WiseSocial App

---------------------------------------------
```
Request Parameters -
postId: list of IDs e.g. 13123,123123,423432
wisers_kol_id: KOL ID of Wisers


Sample Request:
{
    "wisers_kol_id":1,
    "postId": ["13123","doc123123","doc423432"]
}
```
-------------------------------------

````
Response Parameters
    postList: List
    totalCount: String num

Response Class

Model Schema
{
    "totalCount": "",
    "postList": [
        {
            "id": "",
            "content": "",
            "comment": {
                "content": "",
                "time": "",
                "docId": "",
                "sinceId": "",
                "authorImageUrl": "",
                "authorName": ""
            },
            "contentType": "",
            "sourceRegion": "",
            "url": "",
            "pubDate": "",
            "viewCount": 0,
            "docId": "",
            "title": "",
            "author": "",
            "blogId": "",
            "media": "VMedia",
            "fansCount": 0,
            "likeCount": 0,
            "shareCount": 0,
            "commentCount": 0,
            "mentionCommentCount": 0,
            "mentionCommentIds": "",
            "loveCount": "",
            "hahaCount": "",
            "wowCount": "",
            "angryCount": "",
            "sadCount": "",
            "authorType": 0,
            "sourceId": "",
            "sourceName": "",
            "authorImageUrl": "",
            "imageCount": 0,
            "thumbImageUrl": "",
            "orgImageUrl": "",
            "approximateTime": "",
            "preciseTime": "",
            "mediaKey": "",
            "orderByDate": "",
            "authorId": "",
            "subTitle": "",
            "subContent": "",
            "midImgUrl": "",
            "allSentimentCount": "",
            "highKeywords": ""
        }
    ],
    "returnCode": "",
    "action": ""
}

returnCode -
50000: Success
40000: Fail
E10001: Timeout
E10004: request param null
```


### Sample Code in Swift

```
public func get_post_with_ids(wisers_kol_id:Int, postId:[String], completionHandler:@escaping (_ result:[[String:AnyObject]]?, _ totalCount:String?) -> Void) {
    if noNetwork {
        return
    }

    let body:[String:Any] = [
        "wisers_kol_id": wisers_kol_id,
        "postId": postId
    ]

    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))

        var request = URLRequest(url: URL(string: "get_post_with_ids", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData

        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let result = jsonDic["postList"] as? [[String:AnyObject]] {
                            completionHandler(result, jsonDic["totalCount"] as? String)
                        }
                        else {
                            completionHandler([], "0")
                        }

                        return
                    }
                    else {
                        checkReturnCode("get_post_with_ids", code: code)
                    }
                }
            }
            else {
                print("get_post_with_ids", error)
            }

            completionHandler(nil, nil)
        }).resume()
    }
    catch {
    }
}
```
