# SNS Backend API - get_interests_industries

## Implementation Notes

### Get sns social interests & industries

---------------------------------------------
```
Request Parameters -
wisers_kol_id: KOL ID of Wisers


Sample Request:
{
    "wisers_kol_id":1
}

-------------------------------------
Response Parameters
    interestList: List
    industryList: List

Response Class

Model Schema
{
    "interestList": [
        {
            "id": 0,
            "name": ""
        }
    ],
    "industryList": [
        {
            "id": 0,
            "name": ""
        }
    ],
    "returnCode": "",
    "action": ""
}

returnCode -
50000: Success
40000: Fail
E10001: Timeout
E10004: request param null
```


### Sample Code in Swift

```
public func get_interests_industries(wisers_kol_id:Int, completionHandler:@escaping (_ interest:[[String:Any]]?, _ industry:[[String:Any]]?) -> Void) {
    if noNetwork {
        return
    }

    let body:[String:Any] = [
        "wisers_kol_id": wisers_kol_id,
    ]

    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))

        var request = URLRequest(url: URL(string: "get_interests_industries", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData

        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let interest = jsonDic["interestList"] as? [[String:Any]], let industry = jsonDic["industryList"] as? [[String:Any]] {
                            completionHandler(interest, industry)
                        }
                        else {
                            completionHandler([], [])
                        }
                        return
                    }
                    else {
                        checkReturnCode("get_interests_industries", code: code)
                    }
                }
            }
            else {
                print("get_interests_industries", error)
            }

            completionHandler(nil, nil)
        }).resume()
    }
    catch {
    }
}
```
