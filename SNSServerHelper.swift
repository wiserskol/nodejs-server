//
//  SNSServerHelper.swift
//  KOL
//
//  Created by Raymond on 10/24/16.
//  Copyright © 2016 Wisers. All rights reserved.
//

import Foundation
import UIKit

var noNetwork = false
var domain = "http://192.168.3.25:8080"

public func get_interests_industries(wisers_kol_id:Int, completionHandler:@escaping (_ interest:[[String:Any]]?, _ industry:[[String:Any]]?) -> Void) {
    if noNetwork {
        return
    }
    
    let body:[String:Any] = [
        "wisers_kol_id": wisers_kol_id,
        ]
    
    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        var request = URLRequest(url: URL(string: "get_interests_industries", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let interest = jsonDic["interestList"] as? [[String:Any]], let industry = jsonDic["industryList"] as? [[String:Any]] {
                            completionHandler(interest, industry)
                        }
                        else {
                            completionHandler([], [])
                        }
                        return
                    }
                    else {
                        checkReturnCode("get_interests_industries", code: code)
                    }
                }
            }
            else {
                print("get_interests_industries", error)
            }
            
            completionHandler(nil, nil)
        }).resume()
    }
    catch {
    }
}

public func get_brands(wisers_kol_id:Int, completionHandler:@escaping (_ result:[[String:AnyObject]]?) -> Void) {
    if noNetwork {
        return
    }
    
    let body:[String:Any] = [
        "wisers_kol_id": wisers_kol_id,
    ]
    
    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        var request = URLRequest(url: URL(string: "get_brands", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let result = jsonDic["brandList"] as? [[String:AnyObject]] {
                            completionHandler(result)
                        }
                        else {
                            completionHandler([])
                        }                        
                        return
                    }
                    else {
                        checkReturnCode("get_brands", code: code)
                    }
                }
            }
            else {
                print("get_brands", error)
            }
            
            completionHandler(nil)
        }).resume()
    }
    catch {
    }
}

public func get_post_list(wisers_kol_id:Int, pageNum:Int, type:String, keyword:String?, completionHandler:@escaping (_ result:[[String:AnyObject]]?, _ totalCount:String?) -> Void) {
    if noNetwork {
        return
    }
    
    let body:[String:Any] = [
        "pageNum": pageNum,
        "pageSize": 20,
        "keyword": keyword ?? "",
        "type": type,
        "wisers_kol_id": wisers_kol_id,
        "industries": ["food","fashion"]
    ]
    
    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        var request = URLRequest(url: URL(string: "get_post_list", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let result = jsonDic["postList"] as? [[String:AnyObject]] {
                            completionHandler(result, jsonDic["totalCount"] as? String)
                        }
                        else {
                            completionHandler([], "0")
                        }
                        
                        return
                    }
                    else {
                        checkReturnCode("get_post_list", code: code)
                    }
                }
            }
            else {
                print("get_post_list", error)
            }
            
            completionHandler(nil, nil)
        }).resume()
    }
    catch {
        
    }
}

public func get_post_with_ids(wisers_kol_id:Int, postId:[String], completionHandler:@escaping (_ result:[[String:AnyObject]]?, _ totalCount:String?) -> Void) {
    if noNetwork {
        return
    }
    
    let body:[String:Any] = [
        "wisers_kol_id": wisers_kol_id,
        "postId": postId
    ]
    
    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))
        
        var request = URLRequest(url: URL(string: "get_post_with_ids", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String {
                    if code == "50000" {
                        if let result = jsonDic["postList"] as? [[String:AnyObject]] {
                            completionHandler(result, jsonDic["totalCount"] as? String)
                        }
                        else {
                            completionHandler([], "0")
                        }
                        
                        return
                    }
                    else {
                        checkReturnCode("get_post_with_ids", code: code)
                    }
                }
            }
            else {
                print("get_post_with_ids", error)
            }
            
            completionHandler(nil, nil)
        }).resume()
    }
    catch {
        
    }
}

func checkReturnCode(_ from:String, code:String) {
    print("Return code", code, "from", from)
    
    if code == "10009" {
        NotificationCenter.default.post(name: Notification.Name.init(rawValue: "UsageOverflow"), object: nil)
    }
    else if code == "E10001" {
        relogin()
    }
    else {
        relogin()
    }
}

func relogin() {
    
}
