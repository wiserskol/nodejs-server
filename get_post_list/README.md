# SNS Backend API - get_post_list

## Implementation Notes
Get sns social post list for KOL user

This API is a modified version from WiseSocial App

---------------------------------------------
```
Request Parameters -
pageNum: pageNum default:1
pageSize: pageSize default:20
keyword: keyword
wisers_kol_id: KOL ID of Wisers
industries: list of 'fashion','food' ....
type: post type, ie. (post) feed / (following) kol / (following) brand


Sample Request:
{
    "keyword":"",
    "pageNum":1,
    "pageSize":20,
    "type":feed,
    "wisers_kol_id":1,
    "industires": "food,fashion",
}
```
-------------------------------------

````
Response Parameters
    postList: List
    totalCount: String num

Response Class

Model Schema
{
    "totalCount": "",
    "postList": [
        {
            "id": "",
            "content": "",
            "comment": {
                "content": "",
                "time": "",
                "docId": "",
                "sinceId": "",
                "authorImageUrl": "",
                "authorName": ""
            },
            "contentType": "",
            "sourceRegion": "",
            "url": "",
            "pubDate": "",
            "viewCount": 0,
            "docId": "",
            "title": "",
            "author": "",
            "blogId": "",
            "media": "VMedia",
            "fansCount": 0,
            "likeCount": 0,
            "shareCount": 0,
            "commentCount": 0,
            "mentionCommentCount": 0,
            "mentionCommentIds": "",
            "loveCount": "",
            "hahaCount": "",
            "wowCount": "",
            "angryCount": "",
            "sadCount": "",
            "authorType": 0,
            "sourceId": "",
            "sourceName": "",
            "authorImageUrl": "",
            "imageCount": 0,
            "thumbImageUrl": "",
            "orgImageUrl": "",
            "approximateTime": "",
            "preciseTime": "",
            "mediaKey": "",
            "orderByDate": "",
            "authorId": "",
            "subTitle": "",
            "subContent": "",
            "midImgUrl": "",
            "allSentimentCount": "",
            "highKeywords": ""
        }
    ],
    "returnCode": "",
    "action": ""
}

returnCode -
50000: Success
40000: Fail
E10001: Timeout
E10004: request param null
```


### Sample Code in Swift

```
public func get_post_list(wisers_kol_id:Int, pageNum:Int, type:String, keyword:String?, completionHandler:@escaping (_ result:[[String:AnyObject]]?, _ totalCount:String?) -> Void) {
    if noNetwork {
        return
    }

    let body:[String:Any] = [
        "pageNum": pageNum,
        "pageSize": 20,
        "keyword": keyword ?? "",
        "type": type,
        "wisers_kol_id": wisers_kol_id,
        "industries": ["food","fashion"]
    ]

    do {
        let bodyData = try JSONSerialization.data(withJSONObject: body, options: JSONSerialization.WritingOptions(rawValue: 0))

        var request = URLRequest(url: URL(string: "get_post_list", relativeTo: URL(string: domain)!)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField:"Content-Type")
        request.httpBody = bodyData

        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        URLSession.shared.dataTask(with: request, completionHandler: { (data, res, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false

            if error == nil {
                if let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any],
                    let jsonDic = json, let code = jsonDic["returnCode"] as? String 
                {
                    if code == "50000" {
                        if let result = jsonDic["postList"] as? [[String:AnyObject]] {
                            completionHandler(result, jsonDic["totalCount"] as? String)
                        }
                        else {
                            completionHandler([], "0")
                        }

                        return
                    }
                    else {
                        checkReturnCode("get_post_list", code: code)
                    }
                }
            }
            else {
                print("get_post_list", error)
            }

            completionHandler(nil, nil)
        }).resume()
    }
    catch {
    }
}
```
